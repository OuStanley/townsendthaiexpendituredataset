This =var= directory is meant to contain files which are *outputs*
from some other program or process, perhaps operating on files in
[[../OriginalData]].  No files in this directory (other than this one)
should be added to the repository; the idea is that anything here can
be rebuilt from primitive data and code found elsewhere.
